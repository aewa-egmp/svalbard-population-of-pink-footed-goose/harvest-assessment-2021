#-------------------------------------------------------------------------------
# Integrated population model pink-footed goose
# w/embedded regression for annual reproduction
#-------------------------------------------------------------------------------
rm(list=ls())
options(max.print = 100000)
library(jagsUI)

setwd('C:\\Users\\fjohn\\OneDrive\\Documents\\PROJECTS\\European Geese\\A Pink-footed geese\\O 2021 Assessment\\Assessment')
source('C:\\Users\\fjohn\\OneDrive\\Documents\\MyFunctions.R')

#load('PFG IPM fixed-yr-bias 26May2021.RData')

#MyFunctions
# Beta method of moments
MOM.beta=function(mu,var) {
  sum_ab=(mu*(1-mu)/var)-1
  a=sum_ab*mu
  b=sum_ab*(1-mu)
  answer=list(a,b)
  names(answer)[[1]]="a"
  names(answer)[[2]]="b"
  return(answer)
}

# Function to convert mean and sd to lognormal scale
lognorm.pr <- function(expect,stdev){
  prior.sig <- log(1+(stdev^2/expect^2))
  prior.se <- sqrt(prior.sig)
  prior.est <- log(expect)-(prior.sig*0.5)
  result <- c(prior.est,prior.se)
  return(result)
}

# Inverse logistic
expit = function(xx) { 1/(1 + exp(-xx)) }



####################################################################################
### COMPILE DATA ###################################################################
####################################################################################
alldata = read.csv('IPMdata2021_25May2021.csv')
data = subset(alldata, year>1991) # to eliminate first year (1991) with no May LP est
ndx = nrow(data)

# get May LP pop est's and sample var's (scale and round by 1000)
mlp = round(data$maylp/1000,0)
msigma = round(sqrt(data$maylpvar/1000^2),0)
misigma2 = 1/msigma^2
mc = round(data$nm/1000,0)

nyears = length(data$year[-ndx])
days = data$days[-ndx]/10 # to place covariate closer to 0
nc = round(data$nn[-ndx]/1000,0)
hnor = round(data$hnor[-ndx]/100,0) # workaround to get larger CV in Poisson likelihood
hden = round(data$hden[-ndx]/100,0)
P = data$P[-ndx]
p = data$p[-ndx]
W = data$W[-ndx]
w = data$w[-ndx]

#... prior for initial May population size (1992)
m.init = lognorm.pr(31,7) # 95% CI = 20-47 thousand, with mean = mlp(1992)
lmean.m = m.init[1]
ltau.m = 1/m.init[2]^2

# size and var for priors
sizes = data[-ndx,15:16]
muS = sizes$theta
varS = sizes$var

jags.data <- list(nyears = nyears, mlp = mlp, misigma2 = misigma2, mc = mc, nc = nc, 
                   hnor = hnor, hden = hden,
                   P = P, p = p, muS = muS, varS = varS,
                   W = W, w = w,
                   lmean.m = lmean.m, ltau.m = ltau.m,
                   days=days) 
jags.data

#########################################################################################
### PREPARE MODEL #######################################################################
#########################################################################################
# initial values
init.vals <- function(){list(dv=2,cr=8/39,coef=0.92,
                             beta0=-2,beta1=0.5)}

# parameters to monitor
parms = c('Nm','Nn',
          'tau.m','tau.n',
          'bias',
          'theta','s',
          'r','size','psi','prob','exp.p',
          'beta0','beta1',
          'harvn','harvd','H',
          'hn','hd','ho','h','alpha','exp.w',
          'dv','cr','coef','crnow',
          'deviance')

# Define model 
cat(file="pfg.mod",
    "model {
      
# PRIORS and fixed values ------------------------------------------------------------

#...residual errors for pop observations
  tau.m ~ dgamma(0.001,0.001) # May count residual error
  tau.n ~ dgamma(0.001,0.001) # Nov count residual error

#...November count bias
#......random-year effect
 # mu.bias ~ dnorm(lbiasmu,lbiastau) 
 # sigma.bias ~ dunif(0,0.2)
 # tau.bias = 1/sigma.bias^2
 # for (t in 1:nyears) {
 #     epsilon[t] ~ dnorm(0,tau.bias)
 #     log(bias[t]) = mu.bias + epsilon[t]
 # }
#......fixed-year effect
# for (t in 1:nyears) { bias[t] ~ dnorm(0.87,1/0.05^2) } # from random-year model: conv. issues
 for (t in 1:nyears) { bias[t] ~ dunif(0.6,1.1) } 

#...overdispersion for reproductivity betabinomial
#......year-specfic values 
 for (t in 1:nyears) {
      size[t] ~ dgamma(muS[t]^2/varS[t],muS[t]/varS[t])
 }

#...differential vulnerability
   mud <- 2.22      # weighted mean & SS from 1994-96,2008-09,2012-17 (Kevin)
   sigmad <- 0.033  # SS
   dv ~ dgamma(mud^2/sigmad,mud/sigmad)

#...crippling rates
#.....base rate from 31 retrieved and 8 unretrieved in 1996-97 spyblind
 cr ~ dbeta(8,31)

#.....exponential rate of decline based on Clausen 2017 EcoInd
 coef ~ dbeta(387,32)
 for (t in 1:nyears) {
  rate[t] <- coef^(t-1)
  }

#...demographic rates
 for (t in 1:nyears) {
      theta[t] ~ dbeta(49,7)
      hn[t] ~ dunif(0,0.3)           # harvest rate of adults in Norway
      hd[t] ~ dunif(0,0.3)           # harvest rate of adults in Denmark
      ho[t] ~ dunif(0,0.3)           # harvest rate of adults in Denmark, Sept-Oct
      }

#...intital May population size
  Nm[1] ~ dlnorm(lmean.m,ltau.m)
  log.nm[1] <- log(Nm[1])

#...regression coefficients for reproduction = f(thaw days)
  beta0 ~ dnorm(0,0.001)
  beta1 ~ dnorm(0,0.001)

# System process ----------------------------------------------------------------------------------- 

 for (t in 2:(nyears+1)) {
     Nm[t] <- max(0.01,Nm[t-1] * (theta[t-1]*(1-hn[t-1]-hd[t-1]) + r[t-1]*theta[t-1]*(1-dv*hn[t-1]-dv*hd[t-1])))
     log.nm[t] <- log(Nm[t])
     }

  for (t in 1:nyears) {
      Nn[t] <- max(0.01,Nm[t] * pow(theta[t],6/12) * (1-(hn[t]+ho[t]) + r[t]*(1-dv*hn[t]-dv*ho[t])))
      }


# Likelihood for pops ------------------------------------------------------------------------------

#... May LP population estimates [missing 1999] (assumes normal sampling error)
  for (t in 2:7) {
      mlp[t] ~ dnorm(Nm[t],misigma2[t])
      }
  for (t in 9:(nyears+1)) {
      mlp[t] ~ dnorm(Nm[t],misigma2[t])
     }

#... May counts 2010 onward (assumes lognormal residual error)
  for (t in 19:(nyears+1)) {
      mc[t] ~ dlnorm(log.nm[t],tau.m)
      }

#... November counts 1992 onward  (assumes lognormal residual error)
  for (t in 1:nyears) {
      nc[t] ~ dlnorm(log(Nn[t]*bias[t]),tau.n)
      }


# Likelihood for pre-November harvest in Denmark-----------------------------------------------------
  for (t in 1:nyears) {
      alpha[t] <- min(0.99999,max(0.00001,ho[t]/hd[t]))
      w[t] ~ dbin(alpha[t],W[t])
      exp.w[t] = alpha[t]*W[t] # expectation for w
      }


# Likelihood for proportion of young in November---------------------------------------------
  for (t in 1:nyears) {
      logit.gamma[t] <- beta0 + beta1*days[t]
      gamma[t] <- exp(logit.gamma[t])/(1+exp(logit.gamma[t]))
      r[t] <- gamma[t]/(1-gamma[t])
      psi[t] <- r[t]*(1-dv*hn[t]-dv*ho[t]) / (1-(hn[t]+ho[t]) + r[t]*(1-dv*hn[t]-dv*ho[t]))
      prob[t] ~ dbeta(size[t]*psi[t],size[t]*(1-psi[t]))
      p[t] ~ dbin(prob[t],P[t])
      exp.p[t] = prob[t]*P[t] # expectation for p
      }


# Likelihood of harvests------------------------------------------------------------------------------
  for (t in 1:nyears) {
 #... Norway   
      harvn[t] <- 10*(max(0.00001,hn[t]*Nm[t]*pow(theta[t],4/12)*(1+dv*r[t])))
      hnor_lam[t] = harvn[t]*(1-cr*rate[t])
      hnor[t] ~ dpois(hnor_lam[t])

 #... Denmark
      harvd[t] <- 10*(max(0.00001,hd[t]*Nm[t]*pow(theta[t],4/12)*(1+dv*r[t])))
      hden_lam[t] = harvd[t]*(1-cr*rate[t])
      hden[t] ~ dpois(hden_lam[t])
      }


# Derived parms --------------------------------------------------------------------------------------
 for (t in 1:nyears) {
  H[t] <- Nm[t]*pow(theta[t],4/12)*((hn[t]+hd[t]) + r[t]*(dv*hn[t]+dv*hd[t]))
  h[t] <- hn[t]+hd[t]
  s[t] <- theta[t]*(1-h[t])
  }
  crnow <- cr*rate[nyears]

} # End model
")

#########################################################################################
## RUN JAGS #############################################################################
#########################################################################################
ni <- 400000
nb <- 350000
nc <- 3 # number of chains

start = proc.time()[3]
pfg.out <- jags(jags.data, init.vals, parms, "pfg.mod", n.chains = nc, n.burnin = nb, 
                 n.iter = ni, parallel = TRUE)
(time = (proc.time()[3]-start)/60)

save.image("PFG IPM fixed-yr-bias 26May2021.RData")
 
