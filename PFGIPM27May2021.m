clear all;
close all;
format long;

popwt = 1.00; % weight on population objective; weight on harvest obj is 1-popwt
goal = 60;

% Discretization of state and control variables
Nmin = 0; Nmax = 100; Ninc = 1;  % population size in May
Dmin = 0; Dmax = 30;  Dinc = 2;   % thaw days  
Hmin = 0; Hmax = 50;  Hinc = 1;   % harvest quotas (optimal harvests implicitly include crippling loss)

% Shocks
%... thaw days 2000 ->
e1 = (Dmin:Dinc:Dmax)';   
w1 = [0.0279 0.0706 0.0981 0.1132 0.1181 0.1152 0.1064 0.0936 0.0784 0.0624 0.0467 0.0324 0.0203 0.0110 0.0046 0.0011]';
%sum(w1)

%... theta (naturual survival)
% expected value
Etheta = 0.911;
e2 = [0.87753 0.89445 0.90850 0.92137 0.93424]';   
w2 = [0.00482 0.14355 0.50878 0.31729 0.02556]';
%sum(w2)

% relation between preseason age ratio and thaw days
[e34,w34] = qnwnorm([3 3],[-1.6729998  0.3284436],[0.009056233 -0.006904077; -0.006904077  0.006272968]);
Ebeta0 = w34'*e34(:,1); Ebeta1 = w34'*e34(:,2);

% diff vulnerability
Edv = 1.942819;
e5 = [1.547 1.765 1.979 2.209 2.484]';
w5 = [0.022 0.302 0.517 0.154 0.005]';
%sum(w4)

% n = 5; % number of shocks for partial observability
% sigma = 0.052; % sqrt(MSE)
% [x,y] = qnwlogn(n,0,sigma^2);
e6 = 1; w6 = 1; % Y -> N

e = rectgrid(e1,e2,e34,e5,e6);
grid = rectgrid(w1,w2,w34,w5,w6);
w = (prod(grid,2));  
sum(w); 

% Define state and action vectors
N = (Nmin:Ninc:Nmax)'; 
D = (Dmin:Dinc:Dmax)';
S = rectgrid(N,D);
H = (Hmin:Hinc:Hmax)';
X = rectgrid(S,H);  % State,Action combination

% state transition functions
rfcn = @(D,beta0,beta1) (1./(1+exp(-(beta0+beta1.*D./10)))) ./ (1 - (1./(1+exp(-(beta0+beta1.*D./10)))));
PFtran = @(N,D,H,theta,beta0,beta1,dv) max(0,( ...
  N.*theta.*( ...
  ( 1-H./(N.*theta.^(4/12).*(1+dv.*rfcn(D,beta0,beta1)))) + ...
  rfcn(D,beta0,beta1).*(1-dv.* ...
      H./(N.*theta.^(4/12).*(1+dv.*rfcn(D,beta0,beta1)))) ...
   ) ...
));

%... Paul's function coding for ease of interpretation (not supported in my version of Matlab; need a separate function file)
% function PF=PFtran(N,D,H,theta,beta0,beta1,dv) 
% xx = 1./(1+exp(-(beta0+beta1.*D./10)));
% rfcn = xx ./ (1 - xx);
% HH = H./(N.*theta.^(4/12).*(1+dv.*rfcn));
% PF = max(0, N.*theta.*( (1-HH) + rfcn.*( 1-dv.*HH ) ) );
% end

Dtran = @(N,D,H) ones(size(N,1),1); % for every state/action return "1"; next year's days are given by 1 x e1, with probabilities w1

% compute transition matrices
%pf = @(X,e) [PFtran(X(:,1),X(:,2),X(:,3),e(:,2),e(:,3),e(:,4),e(:,5)) Dtran(X(:,1),X(:,2),X(:,3)).*e(:,1)];
 pf = @(X,e) [max(0, PFtran(X(:,1).*e(:,6),X(:,2),X(:,3),e(:,2),e(:,3),e(:,4),e(:,5))) Dtran(X(:,1).*e(:,6),X(:,2),X(:,3)).*e(:,1)];
cleanup = 0; % 0 = none; 1 = neg values set to zero, renormalized; 2 = snap to grid
tic;
Stran = g2P(pf,{N,D},X,e,w,cleanup);
minutes = toc/60

% objective function (constrain harvest <= N)
 ndx = (X(:,3)>X(:,1) & X(:,3)>0);
 Hutil = X(:,3)./max(H);
 Hutil(ndx) = -inf;
 
 EN = PFtran(X(:,1),X(:,2),X(:,3),Etheta,Ebeta0,Ebeta1,Edv); % expectation of next pop
 poputil = 1./(1+exp(-(10-(abs(EN-goal))))); % utility of next pop
 %poputil = 1./(1+exp(-(10-(abs(X(:,1)-goal))))); % utility of current pop
 R = popwt*poputil + (1-popwt)*Hutil; 

% compute policy
I = getI(X,1:2);

% INFINITE TIME HORIZON, DELTA = 1; RELATIVE (AVERAGE) VALUE
% modelfull=struct('discount',1.0,'Ix',I);
% options=struct('algorithm','f','print',2,'tol',5e-6,'relval',1);

% INFINITE TIME HORIZON, DISCOUNTED; MODIFIED POLICY ITERATION
%  modelfull=struct('d',0.99,'Ix',I);
%  options=struct('print',2);
 
% INFINITE TIME HORIZON VANISHING DISCOUNT
  modelfull=struct('d',1.0,'Ix',I);
  options=struct('algorithm','i','vanish',0.99999,'print',2);

modelfull.R = R;
modelfull.P = Stran;
results = mdpsolve(modelfull,options);

pstar = modelfull.P(:,results.Ixopt);
Sfreq=longrunP(pstar);
M=marginals(Sfreq,S);
Epop = M{1}'*N
Edays = M{2}'*D
Eval = Sfreq'*R(results.Ixopt)

policy=[X(results.Ixopt,:) results.v];
dlmwrite('policy_27May2021.csv', policy,'precision','%.15f');
save('policy27May2021')


  
  